/// Theory

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// Через document.createElement.
// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Первый параметр отвечает за то, куда нужно вставить ваш код. Второй это сам код.
// beforeBegin – перед elem.
// afterBegin – внутрь elem, в самое начало.
// beforeEnd – внутрь elem, в конец.
// afterEnd – после elem.
// 3. Як можна видалити елемент зі сторінки?
// Через метод Element.remove() или querySelector


/// Practice

function createList(array) {
    const elementUl = document.createElement("ul");
    document.body.appendChild(elementUl);

    let elementsLi = array.map(item => {
        let nestedUl = "";
        if (!(typeof (item) === "object")) {
            return `<li>${item}</li>`;
        } else {
            if (Array.isArray(item)) {
                for (let nestedItem of item) {
                    nestedUl += `<li>${nestedItem}</li>`;
                }
            } else {
                for (let nestedItem in item) {
                    nestedUl += `<li>${item[nestedItem]}</li>`;
                }
            }

            return `<ul>${nestedUl}</ul>`
        }

    });

    for (let li of elementsLi) {
        elementUl.innerHTML += li;
    }

}

createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", ["NY", "LA", "DC", "CG"], {name: "123"}]);

let button = document.createElement("button");
button.innerText = "Clear";
divka = document.createElement("div");
document.body.appendChild(divka);
document.body.appendChild(button);
button.setAttribute("onclick", "clearPage()");
function clearPage() {
    let secs = 4;
    let timer = setInterval(tick,1000)
    function tick(){
        divka.innerText = "left "+(--secs)+" seconds";
    }
    setTimeout(function() {
        document.querySelector("body").outerHTML = "";
    }, 4000);
};


